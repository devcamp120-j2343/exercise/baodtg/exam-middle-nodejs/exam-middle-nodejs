//Khai báo thư viện express:
const express = require('express');
var cors = require('cors')

const path = require("path");

//Import thư viện mongoose
const mongoose = require('mongoose');

//import models
const userModel = require('./app/models/userModel');
const contactModel = require('./app/models/contactModel')

//import Router
const { userRouter } = require('./app/routers/userRouter');
const { contactRouter } = require('./app/routers/contactRouter');

const { dateMiddleware, getUrlMiddleware } = require('./app/middlewares/vaccinationMiddleware');


//Khai báo app:
const app = express();

app.use(cors())


app.use(express.json())

//Khai báo port:
const port = 8000;

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", dateMiddleware, getUrlMiddleware, (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/index.html"));
})

// app.get("/admin/users",(request, response) => {
//     console.log(__dirname);
//     response.sendFile(path.join(__dirname + "/admin/user/userAdmin.html"));
// })

app.get("/admin/contacts", (request, response) => {
    response.sendFile(path.join(__dirname + "/admin/contact/contactAdmin.html"))
})

app.use(express.static(__dirname + "/views"))


mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Vaccination")
    .then(() => console.log("Connected to Mongo Successfully"))
    .catch(error => handleError(error));


app.use("/", userRouter)
app.use("/", contactRouter)




app.listen(port, () => {
    console.log("Chạy project trên cổng 8000")
})
