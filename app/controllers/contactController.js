//import thư viện userModel
const { default: mongoose } = require('mongoose');
const contactModel = require('../models/contactModel');

//create new contact:
const createContact = async (req, res) => {
    //B1: thu thập dữ liệu:
    const { email } = req.body;
    //B2: validate dữ liệu:
    if (!email) {
        return res.status(400).json({
            status: `Bad request`,
            message: `email is required!`
        })
    }
    //B3: thực thi model
    let createContactData = {
        _id: new mongoose.Types.ObjectId(),
        email: email
    }
    try {

        const createdContact = await contactModel.create(createContactData);
        return res.status(201).json({
            status: `Create new contact successfully`,
            data: createdContact
        })

    }
    catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}

//get all contacts:
const getAllContacts = async (req, res) => {
    //B1: Thu thập dữ liệu:
    try {
        const contactsList = await contactModel.find();
        if (contactsList || contactsList.length > 0) {
            return res.status(200).json({
                status: "Get all contacts successfully",
                data: contactsList
            })
        } else {
            return res.status(404).json({
                status: "Not found any contacts",
                data: contactsList
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

//get contact by id:
const getContactById = async (req, res) => {
    const contactId = req.params.contactId;
    if (!mongoose.Types.ObjectId.isValid(contactId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Contact Id is invalid"
        })
    }
    try {
        const contactFoundById = await contactModel.findById(contactId);
        if (contactFoundById) {
            return res.status(200).json({
                status: `Get contact by id: ${contactId} is successfully`,
                data: contactFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any contacts with id: ${contactId}`,
                data: contactFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

//Update contact by Id:
const updateContactById = async (req, res) => {
    const contactId = req.params.contactId;
    const { email } = req.body;
    if (!mongoose.Types.ObjectId.isValid(contactId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Contact Id is invalid"
        })
    }
    const updateContactData = {
        email
    }
    try {
        const updatedContact = await contactModel.findByIdAndUpdate(contactId, updateContactData);
        if (updatedContact) {
            return res.status(200).json({
                status: `Update contact by id: ${contactId} is successfully`,
                data: updatedContact
            })
        } else {
            return res.status(404).json({
                status: `Not found any contacts with id: ${contactId}`,
                data: updatedContact
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}
//Delete contact by id:
const deleteContactById = async (req, res) => {
    const contactId = req.params.contactId;
    if (!mongoose.Types.ObjectId.isValid(contactId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Contact Id is invalid"
        })
    }
    try {
        const deletedContact = await contactModel.findByIdAndDelete(contactId);
        if (deletedContact) {
            return res.status(204).json({
                status: `Delete contact by id: ${contactId} is successfully`,
                data: deletedContact
            })
        } else {
            return res.status(404).json({
                status: `Not found any contacts with id: ${contactId}`,
                data: deletedContact
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

module.exports = { createContact, getAllContacts, getContactById, updateContactById, deleteContactById }
