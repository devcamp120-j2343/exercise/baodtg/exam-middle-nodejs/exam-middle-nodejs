//import thư viện userModel
const { default: mongoose } = require('mongoose');
const userModel = require('../models/userModel');

//create new user:
const createUser = async (req, res) => {
    //B1: thu thập dữ liệu:
    const { fullName, phone, status } = req.body;
    //B2: validate dữ liệu:
    if (!fullName) {
        return res.status(400).json({
            status: `Bad request`,
            message: `fullName is required!`
        })
    }

    if (!phone) {
        return res.status(400).json({
            status: `Bad request`,
            message: `phone is required!`
        })
    }
    //B3: thực thi model
    let createUserData = {
        _id: new mongoose.Types.ObjectId(),
        fullName: fullName,
        phone: phone,
        status: status
    }
    try {
        const createdUser = await userModel.create(createUserData);
        return res.status(201).json({
            status: `Create new user successfully`,
            data: createdUser
        })
       
    }
    catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}

//get all users:
const getAllUsers = async (req, res) => {
    try {
        const usersList = await userModel.find()
        if (usersList || usersList.length > 0) {
            return res.status(200).json({
                status: `Get all users successfully!`,
                data: usersList
            })
        } else {
            return res.status(404).json({
                status: `Not found any users`,
                data
            })
        }

    }
    catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}

//Get user by Id:
const getUserById = async (req, res) => {
    //B1: thu thap du lieu
    const userId = req.params.userId;
    //B2: validate du lieu:
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `User Id is invalid`
        })
    }
    try {
        const userFoundById = await userModel.findById(userId);
        if (userFoundById) {
            return res.status(200).json({
                status: `Get user by id successfully`,
                data: userFoundById
            })
        } else {
            return res.status(404).json({
                status: `Do not found any users`,
                
            })
        }
    }
    catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}

//Update user by id
const updateUserById = async (req, res) => {
    const userId = req.params.userId;
    const { fullName, phone, status } = req.body;
    //B2: kiểm tra dữ liệu:
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `userId in invalid!`
        })
    }

    if (!fullName) {
        return res.status(400).json({
            status: `Bad request`,
            message: `fullName is required!`
        })
    }

    if (!phone) {
        return res.status(400).json({
            status: `Bad request`,
            message: `phone is required!`
        })
    }

    //B3: thực thi model:
    try {
        let updateUserData = {
            fullName: fullName,
            phone: phone,
            status: status
        }
        const userUpdated = await userModel.findByIdAndUpdate(userId, updateUserData)
        if (userUpdated) {
            return res.status(200).json({
                status: "Update user successfully",
                data: userUpdated
            })
        } else {
            return res.status(404).json({
                status: `Not found any user`
            })
        }
    }
    catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }

}

const deleteUserById = async (req, res) => {
    let userId = req.params.userId;
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: `Bad request`,
            message:"Id is invalid!"
        })
    }
    try {
        const userDeleted = await userModel.findByIdAndDelete(userId)
        if (userDeleted) {
            return res.status(204).json({
                status: 'Delete user by id successfully',
                data: userDeleted
            })
        } else {
            return res.status(404).json({
                status: 'Not found any user',
                
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }

}

module.exports = { createUser, getAllUsers, getUserById, updateUserById, deleteUserById }
