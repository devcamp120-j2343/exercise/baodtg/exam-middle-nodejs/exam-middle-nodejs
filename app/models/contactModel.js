//B1: khai báo thu viện mongoose:
const mongoose = require('mongoose');

//B2: khai báo thư viện Schema của mongoose;
const Schema = mongoose.Schema;

//B3: tạo đối tượng Schema bao gồm các thuộc tính của collection trong mongoDB:

const contactSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    email: {
        type: String,
        required: true
    }
},
{ timestamps: true }
)


//B4: export schema ra model
module.exports = mongoose.model('contact', contactSchema)