//B1: khai báo thu viện mongoose:
const mongoose = require('mongoose');

//B2: khai báo thư viện Schema của mongoose;
const Schema = mongoose.Schema;

//B3: tạo đối tượng Schema bao gồm các thuộc tính của collection trong mongoDB:
const userSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        required: true,
        unique: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    status: {
        type: String,
        default: "Level 0"
    },

},
    { timestamps: true }
);

// const contactSchema = new Schema({
//     _id: mongoose.Types.ObjectId,
//     email: {
//         type: String,
//         require: true
//     }

// })


//B4: export schema ra model
module.exports = mongoose.model('user', userSchema)