//Khai báo thư viện express:
const express = require('express');
const { createUser, getAllUsers, getUserById, updateUserById, deleteUserById } = require('../controllers/userController');
const path = require("path");

//Khai báo router
const userRouter = express.Router();

//create new user:
userRouter.post("/users", createUser)

//get All users:
userRouter.get("/users", getAllUsers)

//get user by id
userRouter.get("/users/:userId", getUserById)

//update user by id
userRouter.put("/users/:userId", updateUserById)

//delete user by id
userRouter.delete("/users/:userId", deleteUserById)

userRouter.get("/admin/users", (request, response) => {
    response.sendFile(path.join(__dirname + "../../../admin/user/userAdmin.html"))
})
userRouter.use(express.static(__dirname + "../../../admin/user"));



module.exports = { userRouter }