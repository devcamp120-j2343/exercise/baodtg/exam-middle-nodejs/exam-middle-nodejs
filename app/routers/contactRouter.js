//Khai báo thư viện express:
const express = require('express');
const path = require("path");

const { createContact, getAllContacts, getContactById, updateContactById, deleteContactById } = require('../controllers/contactController');
//Khai báo router
const contactRouter = express.Router();

//create new user:
contactRouter.post("/contacts", createContact)

//get All users:
contactRouter.get("/contacts", getAllContacts )

//get user by id
contactRouter.get("/contacts/:contactId", getContactById)

//update user by id
contactRouter.put("/contacts/:contactId", updateContactById)

//delete user by id
contactRouter.delete("/contacts/:contactId", deleteContactById)

contactRouter.get("/admin/contacts", (request, response) => {
    response.sendFile(path.join(__dirname + "../../../admin/contact/contactAdmin.html"))
})
// contactRouter.use(express.static(__dirname + "../../../admin/contact/contactAdmin.html"));



module.exports = { contactRouter }