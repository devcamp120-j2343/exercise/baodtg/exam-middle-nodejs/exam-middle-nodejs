/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://localhost:8000/";

let gUserData = {
    fullName: "",
    phone: ""
}
let gUserEmail = {
    email: ""
};
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
//thục thi nút Get Vaccine
$(document).on("click", "#btnGetVaccine", onBtnGetVaccine = () => {
    window.scrollTo(0, 2000)
});
//thục thi nút Help Center

$(document).on("click", "#btn-help-center", onBtnGetVaccine = () => {
    window.scrollTo(0, 5500)
});

$("#div-reg-form").on("click", "#btn-verify", (paramPhoneNumber) => {
    //B1: thu thập dữ liệu:
    paramPhoneNumber = $("#inp-reg-phone").val().trim();
    //B2: validate:
    let phoneValid = checkPhoneNumber(paramPhoneNumber)
    //B3: xử lý hiển thi:
    if (phoneValid) {
        handlePhoneValid()
    } else {
        handlePhoneInvalid()
    }
})
$("#div-reg-form").on("click", "#btn-submit", () => {
    getVaccineReg(gUserData)

})

//thực thi nút ấn send email:
$("#contact-form").on("click", "#btn-send-email", () => {
    createContact(gUserEmail)
})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
const getVaccineReg = (paramUserData) => {
    //B1: thu thập dữ liệu:
    getData(paramUserData);
    //B2: validate:
    let vDataValid = validateData(paramUserData);
    if (vDataValid) {
        //B3: call api to regist vaccince
        console.log(paramUserData)
        callApiToRegistUserVaccination(gBASE_URL, paramUserData)
    }
}

//hàm send email to create contact:
const createContact = (paramEmail) => {
    //B1: thu thập dữ liệu:
    paramEmail.email = $("#inp-email").val().trim();
    //B2: validate:
    let emailValid = validateEmail(paramEmail.email)
    if(emailValid) {
        // callApiToCreateNewContact(gBASE_URL, paramEmail)
        fetch(gBASE_URL + "contacts", {
            method: "POST",
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
            body: JSON.stringify(paramEmail)
        }
        )
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                console.log(data)
            })
            .catch(
                (error) => {
                    console.log(error)
                }
            )
    }
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm kiểm tra số điện thoại
const checkPhoneNumber = (paramPhoneNumber) => {
    let regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    if (!paramPhoneNumber.match(regex)) {
        alert("Phone number is invalid")
        return false
    }
    return true
}
//hàm validate email:
const validateEmail = (paramEmail) => {
    let emailRegex =/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    ;
    if (!paramEmail.match(emailRegex)) {
        alert("Email is invalid")
        return false
    }
    return true
   

}
//xử lý khi phone valid
const handlePhoneValid = () => {
    $("#btn-verify").val("Verified")
    $("#btn-verify").addClass("btn-success");
}
//xử lý khi phone invalid

const handlePhoneInvalid = () => {
    $("#btn-verify").val("Verify");
    $("#btn-verify").removeClass("btn-success");
}

//B1: thu thập dữ liệu:
const getData = (paramUserData) => {
    paramUserData.fullName = $("#inp-reg-fullname").val();
    paramUserData.phone = $("#inp-reg-phone").val();
}

//B2: validate:
const validateData = (paramUserData) => {
    if (paramUserData.fullName == "") {
        alert("Please input your name!")
        return false
    }
    if ($("#btn-verify").val() == "Verify") {
        alert("Please verify your phone number!")
        return false
    }
    return true
}
//B3: call api to regist vaccince
// async function fetchAPI(url, options) {
//     var response = await fetch(url, options);

//     var data = await response.json();

//     console.log(data);
// }


const callApiToRegistUserVaccination = (url, paramUserData) => {
    fetch(url + "users", {
        method: "POST",
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
        body: JSON.stringify(paramUserData)
    }
    )
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            console.log(data)
        })
        .catch(
            (error) => {
                console.log(error)
            }
        )
    // $.ajax({
    //     url: url + "users",
    //     type: "POST",
    //     data: JSON.stringify(paramUserData),
    //     contentType: "application/json;charset=UTF-8",
    //     success: function (paramRes) {
    //         console.log(paramRes);
    //     },
    //     error: function (paramErr) {
    //         console.log(paramErr.status);
    //     }
    // });
}
// const callApiToCreateNewContact = (url, paramEmail) => {
//     fetch(url + "contacts", {
//         method: "POST",
//         headers: {
//             'Content-type': 'application/json; charset=UTF-8',
//         },
//         body: JSON.stringify(paramEmail)
//     }
//     )
//         .then((response) => {
//             return response.json();
//         })
//         .then((data) => {
//             console.log(data)
//         })
//         .catch(
//             (error) => {
//                 console.log(error)
//             }
//         )
// }

